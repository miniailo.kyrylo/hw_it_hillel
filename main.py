# Напишите функцию для парсинга номерных знаков автомоблей Украины
# (стандарты - AА1234BB, 12 123-45AB, a12345BC) с помощью регулярных выражений.
# Функция принимает строку и возвращает None если вся строка не является номерным знаком.
# Если является номерным знаком - возвращает саму строку.

import re


def firstFindNumberAuto(numberAuto: str) -> str or None:
    reNumberAuto = r'^[A-Z]{2}\d{4}[A-Z]{2}$|' \
                   r'^\d{2} \d{3}-\d{2}[A-Z]{2}$|' \
                   r'^[a-z]\d{5}[A-Z]{2}$'
    reResult = re.search(reNumberAuto, numberAuto)
    reResult = None if reResult is None else numberAuto
    return reResult


numberAuto_1 = "AA1234BB"
numberAuto_2 = "12 123-45AB"
numberAuto_3 = "a12345BC"

print('=========> TASK 1')
print(firstFindNumberAuto(numberAuto_1))
print(firstFindNumberAuto(numberAuto_2))
print(firstFindNumberAuto(numberAuto_3))

# Напишите класс, который выбирает из произвольного текста номерные знаки и возвращает их
# в виде пронумерованного списка с помощью регулярных выражений.


class FindNumberAuto:
    numberAuto_4 = '''Lorem Ipsum is simply dummy text of the printing AA1234BB and typesetting industry.
                   Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                   when an unknown printer took a galley of type and scrambled it to make a type specimen
                   book. It has survived not only five centuries, but also the 12 123-45AB leap into 
                   electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s 
                   with the release of sheets containing Lorem Ipsum passages, and more recently
                   a12345BC with desktop publishing software like Aldus PageMaker including versions of
                   Lorem Ipsum.'''

    def allFindNumberAuto(self: object) -> list:
        reAllNumberAuto = r'\b[A-Z]{2}\d{4}[A-Z]{2}\b|' \
                       r'\b\d{2} \d{3}-\d{2}[A-Z]{2}\b|' \
                       r'\b[a-z]\d{5}[A-Z]{2}\b'
        reResult = re.findall(reAllNumberAuto, self.numberAuto_4)
        reResult = None if reResult == [] else reResult
        return reResult

    def listNum(self: object) -> dict or None:
        dictAutoNumber = dict()
        allFind = self.allFindNumberAuto()
        if allFind is None:
            return None
        else:
            counter = 1
            for item in self.allFindNumberAuto():
                dictAutoNumber.update({counter: item})
                counter += 1
            counter = 1
        return dictAutoNumber


print('=========> TASK 2')
res = FindNumberAuto()
print(res.listNum())

# Создайте репозиторий на GitLab или GitHub. Сохраните отдельной веткой (пусть будет HW14)
# дз по регулярным выражениям
